package com.company;

import java.io.*;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

    static YMCA_Data memberList;
    static YMCA_Data classList;
    static YMCA_Data registerList;
    static Scanner eyes = new Scanner(System.in);
   // the main program
    public static void main(String[] args) {
        memberList = new YMCA_Data(new memberNode()) ;
         classList= new YMCA_Data(new ClassNode()) ;
         registerList= new YMCA_Data(new registerNode()) ;
        int selc = 0;
        startUp();
        while(selc != 5) {
            System.out.println("Area YMCA\n");
            System.out.println("1) Add/Modify Member Information");
            System.out.println("2) Add/Modify Registrations ");
            System.out.println("3) Add/Modify Classes");
            System.out.println("4) Report Section");
            System.out.println("5) Exit the System\n");
            System.out.print("Please Make your selection> ");
            selc = eyes.nextInt();
            switch (selc){
                case 1:
                    memberList.ModifyLists();
                    break;
                case 2:
                    registerList.ModifyLists();
                    break;
                case 3:
                    classList.ModifyLists();
                    break;
                case 4:
                    report();
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Please Enter Valid selection");
                    break;
            }
        }
        end();


    }
   // the report menu
    public static void report(){
        int selection = -12;
        while(selection != 0){
            System.out.println("Report Menu\n==========");
            System.out.println("1) Member Info");
            System.out.println("2) Class Info");
            System.out.println("3) Registration Info");
            System.out.println("4) Member Registration");
            System.out.println("5) Member Gender");
            System.out.println("6) Member Age");
            System.out.println("7) Member not enrolled");
            System.out.println("8) class Registration");
            System.out.println("9) Current Schedule");
            System.out.println("0) Exit");
            System.out.println("Selection> ");
            selection = eyes.nextInt();
            String code;
            switch (selection){
                case 0:
                    break;
                case 1:
                    for (Iterator i = memberList.iterator();i!=null;i = (Iterator)i.next()) {
                        System.out.println(i);
                    }
                    break;
                case 2:
                    for (Iterator i = classList.iterator();i!=null;i = (Iterator)i.next()) {
                        System.out.println(i);
                    }
                    break;
                case 3:
                    for (Iterator i = registerList.iterator();i!=null;i = (Iterator)i.next()) {
                        System.out.println(i);
                    }
                    break;
                case 4:
                    System.out.println("Enter Member Code>");
                     code = eyes.next();
                    for (Iterator i = registerList.iterator();i!=null;i = (Iterator)i.next()) {
                        if(((Comparable)i).compareTo(code+"M") == 1)
                            for (Iterator j = classList.iterator();j!=null;j= (Iterator)j.next()) {
                                if(((ClassNode)j).compareTo(Integer.toString(((registerNode)i).cCode))==1) {
                                    System.out.println(j);
                                    break;
                                }
                            }
                    }
                    break;
                case 5:
                    System.out.println("Enter Gender of interest> ");
                    String g = eyes.next();
                    for (Iterator i = memberList.iterator();i!=null;i = (Iterator)i.next()) {
                        if(((memberNode)i).gender.equalsIgnoreCase(g)) {
                            System.out.println(i);
                        }
                    }
                    break;
                case 6:
                    System.out.println("Enter start age of interest>");
                    int start = eyes.nextInt();
                    System.out.println("Enter end age of interest>");
                    int end = eyes.nextInt();
                    for (Iterator i = memberList.iterator();i!=null;i = (Iterator)i.next()) {
                        int age = ((memberNode)i).age;
                        if(start <= age&& age<= end) {
                            System.out.println(i);
                        }
                    }
                    break;
                case 7:
                    for (Iterator m = memberList.iterator();m!=null;m = (Iterator)m.next()) {
                        boolean reg = false;
                        for (Iterator r = registerList.iterator();r!=null;r = (Iterator)r.next()) {
                            reg = ((memberNode)m).code ==
                                    ((registerNode)r).mCode;

                        }
                        if(!reg)
                            System.out.println(m);
                    }
                    break;
                case 8:
                    System.out.println("Enter Class Code>");
                    code = eyes.next();
                    for (Iterator i = registerList.iterator();i!=null;i = (Iterator)i.next()) {
                        if(((Comparable)i).compareTo(code+"C") == 1)
                            for (Iterator j = memberList.iterator();j!=null;j = (Iterator)j.next()) {
                                if(((memberNode)j).compareTo(Integer.toString(((registerNode)i).mCode))==1) {
                                    System.out.println(j);
                                    break;
                                }
                            }
                    }
                    break;
                case 9:
                    for (Iterator c = classList.iterator();c!=null;c = (Iterator)c.next()) {
                        code = Integer.toString(((ClassNode)c).code);
                        System.out.println(c);
                        for (Iterator i = registerList.iterator();i!=null;i= (Iterator)i.next()) {
                            if(((Comparable)i).compareTo(code+"C") == 1)
                                for (Iterator j = memberList.iterator();j!=null;j= (Iterator)j.next()) {
                                    if(((memberNode)j).compareTo(Integer.toString(((registerNode)i).mCode))==1) {
                                        System.out.println(j);
                                        break;
                                    }
                                }
                        }
                    }
                    break;

                default:
                    System.out.println("Please enter a valid selection");
                    break;
            }
        }
    }
   // the program that reads all information from a file
    public static void startUp(){
        try{
            FileInputStream fstream;
            BufferedReader br;
            StringTokenizer line;
            String strLine;

             fstream = new FileInputStream("C:\\Users\\trae\\GoogleDrive\\Programming Workspace\\LinkLIst\\src\\com\\company\\member.dat");
             br = new BufferedReader(new InputStreamReader(fstream));
            //member
            while ((strLine = br.readLine()) != null)   {
                line = new StringTokenizer(strLine,"#");

                memberList.add(new memberNode(
                        Integer.parseInt(line.nextToken()),
                        line.nextToken(),
                        line.nextToken(),
                        line.nextToken(),
                        Integer.parseInt(line.nextToken()),
                        line.nextToken()
                ));
            }
            br.close();
            fstream = new FileInputStream("C:\\Users\\trae\\GoogleDrive\\Programming Workspace\\LinkLIst\\src\\com\\company\\class.dat");
            br = new BufferedReader(new InputStreamReader(fstream));
            //classNode
            while ((strLine = br.readLine()) != null)   {
                line = new StringTokenizer(strLine,"#");
                classList.add(new ClassNode(
                        Integer.parseInt(line.nextToken()),
                        line.nextToken(),
                        line.nextToken(),
                        null
                ));

            }
            br.close();
            fstream = new FileInputStream("C:\\Users\\trae\\GoogleDrive\\Programming Workspace\\LinkLIst\\src\\com\\company\\register.dat");
            br = new BufferedReader(new InputStreamReader(fstream));
            //register Node
            while ((strLine = br.readLine()) != null)   {
                line = new StringTokenizer(strLine,"#");
                registerList.add(new registerNode(
                        Integer.parseInt(line.nextToken()),
                        Integer.parseInt(line.nextToken()),
                        null
                ));

            }
            //Close the input stream
            br.close();
        } catch (FileNotFoundException e) {
              e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // write all lists to files in the same format as they are stored
    public static void end(){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("C:\\Users\\trae\\GoogleDrive\\Programming Workspace\\LinkLIst\\src\\com\\company\\member.dat", "UTF-8");
            for (Iterator i = memberList.iterator();i!=null;i = (Iterator)i.next()) {
                writer.println(((memberNode)i).line());
            }
            writer.close();
            writer = new PrintWriter("C:\\Users\\trae\\GoogleDrive\\Programming Workspace\\LinkLIst\\src\\com\\company\\class.dat", "UTF-8");
            for (Iterator i = classList.iterator();i!=null;i = (Iterator)i.next()){
                writer.println(((ClassNode)i).line());
            }
            writer.close();
            writer = new PrintWriter("C:\\Users\\trae\\GoogleDrive\\Programming Workspace\\LinkLIst\\src\\com\\company\\register.dat", "UTF-8");
            for (Iterator i = registerList.iterator();i!=null;i = (Iterator)i.next()) {
                writer.println(((registerNode)i).line());
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }
}