package com.company;

/**
 * Created by trae on 4/8/2016.
 */
// an interface i used to standardize some features I should have put more features in this
interface Node<T>{
    void setNext(T x );
    void update(int index,String value);
}