package com.company;

import java.util.Iterator;

/**
 * Created by trae on 4/8/2016.
 */
//the node for register
class registerNode implements Iterator<registerNode>,Node<registerNode>,Comparable<String>   {
    public int mCode=0,cCode=0;
    public registerNode next;
    //empty constructor to used to set up list
    public registerNode(){}
    //main constructor used to add a new item
    public registerNode(int mCode, int cCode, registerNode next) {
        this.mCode = mCode;
        this.cCode = cCode;
        this.next = next;
    }
    //turns object into a string to write it to a file
    public String line(){
        return mCode+"#"+cCode+"#";
    }
    @Override
    // returns whither an element has a next link
    public boolean hasNext() {
        if(next == null)
            return false;
        return true;
    }

    @Override
    // Gets the next registerNode
    public registerNode next() {
        return next;
    }

    @Override
    //used to compare equality of 2 class nodes should have used equal to comparison is done on class codes
    public int compareTo(String o) {
        int code = o.toLowerCase().charAt(o.length()-1) == 'm'?mCode:cCode;
        if(code == Integer.parseInt(o.substring(0,o.length()-1)))
            return 1;
        else
            return 0;

    }

    @Override
    //set the next node in the list
    public void setNext(registerNode x) {
        this.next = x;
    }

    @Override
    // allows the update of properties
    public void update(int index, String value) {
        switch (index){
            case 0:
                mCode = Integer.valueOf(value);
                break;
            case 1:
                cCode = Integer.valueOf(value);
                break;
        }
    }

    @Override
    // converts object to string for printing
    public String toString() {
        return "registerNode{" +
                "mCode=" + mCode +
                ", cCode=" + cCode +
                '}';
    }
}