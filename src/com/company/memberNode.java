package com.company;

import java.util.Iterator;

/**
 * Created by trae on 4/8/2016.
 */
// the node for a member
class memberNode implements Iterator<memberNode>,Node<memberNode>,Comparable<String>  {
    @Override
    // converts object to string for printing
    public String toString() {
        return "memberNode{" +
                "code=" + code +
                ", age=" + age +
                ", type='" + type + '\'' +
                ", fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
    //converts object to string to wrtie to a file
    public String line(){
        return code
                +"#"+
                type
                +"#"+
                fname
                +"#"+
                lname
                +"#"+
                age
                +"#"+
                gender+"#";
    }

    public  int code,age;
    public  String type,fname,lname,gender;
    public memberNode next;
    //empty constructor  used to generate generic list
    public memberNode(){}
    //  constructor used to create node
    public memberNode(int code, int age, String fname,String lname, String type , String gender, memberNode next) {
        this.code = code;
        this.age = age;
        this.fname = fname;
        this.type = type;
        this.lname = lname;
        this.gender = gender;
        this.next= next;
    }
    //  constructor used to create node
    public memberNode(int code,String type, String fname,String lname,int age, String gender) {
        this.code = code;
        this.age = age;
        this.fname = fname;
        this.type = type;
        this.lname = lname;
        this.gender = gender;
        this.next= null;
    }

    @Override
    //returns if the node has  next element
    public boolean hasNext() {
        if(next == null)
            return false;
        return true;
    }

    @Override
    //get the next element
    public memberNode next() {
        return next;
    }

    @Override
    //sets next
    public void setNext(memberNode x) {
        this.next = x;
    }

    @Override
    // used to update properties
    public void update(int index, String value) {
        switch (index){
            case 0:
                code = Integer.valueOf(value);
                break;
            case 1:
                age = Integer.valueOf(value);
                break;
            case 2:
                fname = value;
                break;
            case 3:
                lname = value;
                break;
            case 4:
                type = value;
                break;
            case 5:
                gender = value;
                break;
        }
    }

    @Override
    //used to get equality of a member node and some code
    public int compareTo(String o) {
        int x= Integer.parseInt(o);
        if(code == x)
            return 1;
        else
            return 0;
    }
}
