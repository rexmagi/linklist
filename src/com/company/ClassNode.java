package com.company;

import java.util.Iterator;

/**
 * Created by trae on 4/8/2016.
 */
// the node for a class
class ClassNode implements Iterator<ClassNode>,Node<ClassNode>,Comparable<String>   {
    public int code;
    public String name,date;
    public ClassNode next;
    @Override
    //converts object to string for printing
    public String toString() {
        return "ClassNode{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
    //converts object to string to wrtie to file
    public String line(){
        return code+"#"+name+"#"+date+"#";
    }
    // empty constructor used to setup list
    public ClassNode(){}
    // normal constructor used to create object has next pointer to set next element
    public ClassNode(int code, String name, String date, ClassNode next) {
        this.code = code;
        this.name = name;
        this.date = date;
        this.next = next;
    }
    @Override
    // tells you if the element in the list has a non null next pointer
    public boolean hasNext() {
        if(next == null)
            return false;
        return true;
    }
    @Override
    //gets the next pointer
    public ClassNode next() {
        return next;
    }


    @Override
    //used to compare equality of 2 class nodes should have used equal to comparison is done on class codes
    public int compareTo(String o) {
        if(code == Integer.parseInt(o))
            return 1;
        else
            return 0;
    }

    @Override
    //used to set next element in the list
    public void setNext(ClassNode x) {
        this.next = x;
    }

    @Override
    //used to update feature of class node the int argument will be used to switch on to select feature
    public void update(int index, String value) {
        switch (index){
            case 0:
                code = Integer.valueOf(value);
                break;
            case 1:
                name = (value);
                break;
            case 2:
                date = value;
                break;
        }
    }
}
