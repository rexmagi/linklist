package com.company;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Created by trae on 4/8/2016.
 */
// this is the generic list container it can hold any of the three types of list and can generate the menu to add, delete or modify any type
class YMCA_Data implements Iterable{
    Scanner eyes = new Scanner(System.in);
    String member[] ={"Member Code","Age","First Name","Last Name"," Type","Gender"};
    Object mTypes[] = {12,12,"","","","", new memberNode()};
    String Class[] = {"Class Code","Class Name", " Start Data"};
    Object cTypes[] = {12,"","", new ClassNode()};
    String register[] = {"Member Code", "Class Code"};
    Object rTypes[] = {12, 12,new registerNode() };
    Iterator HeadNode;
    public YMCA_Data(Iterator HeadNode){
        this.HeadNode = HeadNode;
    }
    // the menu to modify a list
    public void ModifyLists(){
        int selection = -12;
        while(selection !=0) {
            int option = 1;
            System.out.println("Add/Modify Menu\n===========");
            System.out.println(Integer.toString(option++)+") Add Node");
            System.out.println(Integer.toString(option++)+") Remove Node");
            if(HeadNode.getClass() != registerNode.class)
                System.out.println(Integer.toString(option++)+") Modify Element");
            System.out.println("0) Exit Menu");
            System.out.print("Selection> ");
            selection = eyes.nextInt();
            switch (selection){
                case 1:
                    Add();
                    break;
                case 2:
                    Remove();
                    break;
                case 3:
                    ModifyElement();
                    break;
                default:
                    System.out.println("Please Enter a Valid selection");
                    break;
            }
        }
    }
   // gets the ith element never used
    public  Iterator get(int x){
        Iterator e = null;
        int counter = 0;
        for (Iterator i = this.iterator();i!=null;i= (Iterator)i.next()) {
            if(counter == x)
                return (Iterator)i;
            counter++;
        }
        return null;
    }
   // creates a new node
    public void Add(){
        ArrayList parms = new ArrayList();
        Iterator tHead = null;
        int counter = 0 ;
        if(HeadNode.getClass() == memberNode.class) {
            for (String x : member) {
                System.out.println("Please Enter " + x + " of new Node");
                if (mTypes[counter] instanceof Integer)
                    parms.add(eyes.nextInt());
                else
                    parms.add(eyes.next());
                counter++;
            }

            tHead = new memberNode((int) parms.get(0), (int) parms.get(1), (String) parms.get(2),
                    (String) parms.get(3), (String) parms.get(4), (String) parms.get(5), (memberNode) HeadNode);
        }
        else if(HeadNode.getClass() == ClassNode.class) {
            for (String x : Class) {
                System.out.println("Please Enter " + x + " of new Node");
                if (cTypes[counter] instanceof Integer)
                    parms.add(eyes.nextInt());
                else
                    parms.add(eyes.next());
                counter++;
            }
            tHead = new ClassNode((int) parms.get(0), (String) parms.get(1),
                    (String) parms.get(2), (ClassNode) HeadNode);
        }
        else if(HeadNode.getClass() == registerNode.class) {
            for (String x :register) {
                System.out.println("Please Enter "+x+" of new Node");
                if(rTypes[counter] instanceof Integer)
                    parms.add(eyes.nextInt());
                else
                    parms.add(eyes.next());
                counter++;
            }
            tHead = new registerNode((int) parms.get(0), (int)parms.get(1),(registerNode)HeadNode);}
        HeadNode = tHead != null ? tHead:HeadNode;
    }
    // adds a new node to the list
    public void add(Node x){
        if(HeadNode.getClass() == registerNode.class){
           if(((registerNode)HeadNode).cCode!=0)
            x.setNext(HeadNode);
            HeadNode = (registerNode)x;}
        if(HeadNode.getClass() == memberNode.class){
            if(((memberNode)HeadNode).gender!=null)
            x.setNext(HeadNode);
            HeadNode = (memberNode)x;
        }
        if(HeadNode.getClass() == ClassNode.class){
            if(((ClassNode)HeadNode).name!=null)
            x.setNext(HeadNode);
            HeadNode = (ClassNode)x;
        }

    }
    //removes a node from the list
    public void Remove(){
        String code = null;
        Iterator prev = null;
        if(HeadNode.getClass() == ClassNode.class) {
            System.out.print("Enter Class code to delete class> ");
            code =eyes.next();
        }
        if(HeadNode.getClass() == memberNode.class) {
            System.out.print("Enter Member code to delete Member> ");
            code =eyes.next();
        }
        if(HeadNode.getClass() == registerNode.class) {
            System.out.print("Enter Member code/Class code to delete a registration\n" +
                    " follow entry with (M or C) to signify member code or class code(1234M)> ");
            code =eyes.next();
        }
        for (Iterator i = this.iterator();i!=null;i= (Iterator)i.next()) {
            if(((Comparable)i).compareTo(code) == 1) {
                if(prev== null) {
                    HeadNode = (Iterator) HeadNode.next();
                    return;
                }
                else {
                    ((Node) prev).setNext((i).next());
                    return;
                }
            }

            prev = (Iterator) i;
        }
    }
    //the menu to update an element
    public void ModifyElement(){
        String code = "";
        int option = 0;
        if(HeadNode.getClass() == ClassNode.class) {
            System.out.print("Enter Class code to Modify class> ");
            code =eyes.next();
        }
        if(HeadNode.getClass() == memberNode.class) {
            System.out.print("Enter Member code to Modify Member> ");
            code =eyes.next();
        }
        if(HeadNode.getClass() == registerNode.class) {
            System.out.print("Enter Member code/Class code to Modify a registration\n" +
                    " follow entry with (M or C) to signify member code or class code(1234M)> ");
            code =eyes.next();
        }
        for (Iterator i = this.iterator();i!=null;i= (Iterator)i.next()) {
            if(((Comparable)i).compareTo(code) == 1 ){
                int selection = -12;
                while(selection != 0) {
                    System.out.println("Modify Menu\n===========");
                    if(HeadNode.getClass() == memberNode.class){
                        for (int j = 0; j < member.length; j++) {

                            System.out.println(Integer.toString(j+1) + ") Update " + member[j]);
                        }}
                    if(HeadNode.getClass() == ClassNode.class){
                        for (int j = 0; j < Class.length; j++) {
                            System.out.println(Integer.toString(j+1) + ") Update " + member[j]);
                        }}
                    if(HeadNode.getClass() == registerNode.class){
                        for (int j = 0; j < register.length; j++) {
                            System.out.println(Integer.toString(j+1) + ") Update " + member[j]);
                        }}
                    System.out.println("0) Exit Menu");
                    System.out.print("Selection> ");
                    selection = eyes.nextInt();
                    if(selection != 0){
                        System.out.println("Please Enter New "+ member[selection-1]);
                        ((Node)i).update(selection-1,eyes.next());

                    }
                }
                return;
            }


        }



    }

    @Override
    //gets pointer from list to iterate through
    public Iterator iterator() {
        return HeadNode;
    }
}